#!/bin/sh

TITLE="$1"
FOLDER="$2"

mkdir "$FOLDER"

/bin/cat <<EOF >"$FOLDER/index.html"
<!DOCTYPE html>
<html>
  <head>
    <title>$TITLE</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Momozor">x
    <!--[if lt IE 8]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser.
       Please <a href="http://browsehappy.com/">upgrade your browser</a> to 
       improve your experience.</p>
    <![endif]-->

    <style>
        canvas {position: fixed; top: 0; left: 0; z-index: 1; }
        #content {position: relative; z-index: 2; }
    </style>
  </head>
  <body>

    <div id="content"></div>
    <script src="../p5/p5.js"></script>
    <script src="compiled.js"></script>
  </body>
</html>
EOF

/bin/cat <<EOF >"$FOLDER/sketch.js"
let setup = () => {
    createCanvas(windowWidth, windowHeight);
}

let windowResized = () => {
    resizeCanvas(windowWidth, windowHeight);
}

let draw = () => {

}
EOF
