let setup = () => {
    createCanvas(windowWidth, windowHeight);
}

let windowResized = () => {
    resizeCanvas(windowWidth, windowHeight);
}

let circleCenterHelper = (radius) => {
    circle(windowWidth / 2, windowHeight / 2, radius);
}

let drawOrbit = () => {
    circleCenterHelper(70); // mercury
    circleCenterHelper(90); // venus
    circleCenterHelper(110); // earth
    circleCenterHelper(170); // mars
    noFill();
}

let drawSun = () => {
    circleCenterHelper(20);
    noFill();
}


let draw = () => {
    drawOrbit();
    drawSun();
}
