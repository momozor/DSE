"use strict";

var setup = function setup() {
    createCanvas(windowWidth, windowHeight);
};

var windowResized = function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
};

var circleCenterHelper = function circleCenterHelper(radius) {
    circle(windowWidth / 2, windowHeight / 2, radius);
};

var drawOrbit = function drawOrbit() {
    circleCenterHelper(70); // mercury
    circleCenterHelper(90); // venus
    circleCenterHelper(110); // earth
    circleCenterHelper(170); // mars
    noFill();
};

var drawSun = function drawSun() {
    circleCenterHelper(20);
    noFill();
};

var draw = function draw() {
    drawOrbit();
    drawSun();
};
